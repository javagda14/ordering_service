package com.aps.ordering.service;

import com.aps.ordering.model.EmailModel;
import com.aps.ordering.model.Employee;
import com.aps.ordering.repository.EmailModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.constraints.Email;
import java.util.List;
import java.util.stream.Collectors;

// serwis odpowiada za zbieranie maili i dodawanie ich do kolejki
@Service
public class MailingService {

    @Autowired
    private EmailModelRepository emailModelRepository;

    /**
     * send email with 1 min schedule
     */
    public void sendQuickEmail(EmailModel emailModel) {
        emailModel.setShouldBeSentQuickly(true);
        emailModelRepository.save(emailModel);
    }

    public void sendEmail(EmailModel emailModel) {
        emailModel.setShouldBeSentQuickly(false);
        emailModelRepository.save(emailModel);
    }

    public List<EmailModel> getAllQuickEmailsToSend() {
        return emailModelRepository.findAllByShouldBeSentQuickly(true);
    }

    public List<EmailModel> getAllEmailsToSend() {
        return emailModelRepository.findAllByShouldBeSentQuickly(false);
    }

    public void hasBeenSent(Long id) {
        emailModelRepository.deleteById(id);
    }

    public List<EmailModel> findAllEntitiesById(Long entityId, String entityTitle) {
        List<EmailModel> emailModels = emailModelRepository.findAllByEmailEntityAndEntityId(entityTitle, entityId);

        emailModels = emailModels.stream().sorted((e1, e2) -> {
            return Long.compare(e1.getId(), e2.getId());
        }).collect(Collectors.toList());

        return emailModels;
    }

    public void remove(List<EmailModel> emailModels) {
        for (EmailModel model : emailModels) {
            emailModelRepository.deleteById(model.getId());
        }
    }

    public boolean checkIfValid(EmailModel mail) {
        return emailModelRepository.existsById(mail.getId());
    }
}
