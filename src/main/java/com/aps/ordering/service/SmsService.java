package com.aps.ordering.service;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class SmsService {
    public static final String ACCOUNT_SID = "AC2d0819f7cd508f8d89c5839e49eaa312";
    public static final String AUTH_TOKEN = "40412711682d4ead314a3bd040adc72d";

    public String sendSms() {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(
                new com.twilio.type.PhoneNumber("+48536199419"),
                new com.twilio.type.PhoneNumber("+48732484037"),
                "Whassssuuuup?")
                .create();

        return message.getSid();
    }
}
