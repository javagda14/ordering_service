package com.aps.ordering.service;

import com.aps.ordering.model.Unit;
import com.aps.ordering.repository.UnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitService {
    @Autowired
    private UnitRepository unitRepository;

    public List<Unit> listAll(){
        return unitRepository.findAll();
    }

    public Unit find(Long unitId) {
        return unitRepository.getOne(unitId);
    }
}
