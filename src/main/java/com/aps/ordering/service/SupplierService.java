package com.aps.ordering.service;

import com.aps.ordering.model.EventFactory;
import com.aps.ordering.repository.ApplicationEventRepository;
import com.aps.ordering.repository.SupplierRepository;
import com.aps.ordering.model.Employee;
import com.aps.ordering.model.Supplier;
import com.aps.ordering.model.dto.SupplierFormDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SupplierService {

    @Value("${product.address.separator}")
    private String SEPARATOR;

    @Autowired
    private ApplicationEventRepository applicationEventRepository;

    @Value("${supplier.default.approved}")
    private Boolean defaultApproved;

    @Autowired
    private SupplierRepository supplierRepository;

    @Autowired
    private LoginService loginService;

    public List<Supplier> findAll() {
        return supplierRepository.findAll();
    }

    public boolean add(SupplierFormDto dto) {
        Supplier supplier = new Supplier();
        supplier.setAwaitingApproval(!defaultApproved);
        supplier.setCreator(loginService.getLoggedInUser().get());
        supplier.setName(dto.getName());
        supplier.setAddress(dto.getAddressName() + SEPARATOR + dto.getAddressStreet() + SEPARATOR + dto.getAddressCode() + SEPARATOR + dto.getAddressCity());
        supplier.setUrl(dto.getUrl());

        supplierRepository.save(supplier);

        return true;
    }

    public boolean remove(Long id, Employee currentUser) {
        Optional<Supplier> supplierOptional = supplierRepository.findById(id);
        if (supplierOptional.isPresent()) {
            if(loginService.isAdmin() || supplierOptional.get().getCreator().getId() == currentUser.getId()){
                applicationEventRepository.saveAndFlush(EventFactory.eventMessage("User removed supplier :" + supplierOptional.get(), currentUser.getUsername()));
                supplierRepository.deleteById(id);
                return true;
            }
        } else {
            applicationEventRepository.saveAndFlush(EventFactory.eventMessage("User tried to remove non existing supplier.", currentUser.getUsername()));
        }
        return false;
    }

    public Optional<Supplier> getSupplierById(Long id) {
        return supplierRepository.findById(id);
    }

    public Optional<Supplier> modify(Long id, SupplierFormDto dto, Employee currentUser) {
        Optional<Supplier> supplierOptional = getSupplierById(id);
        if (supplierOptional.isPresent()) {
            Supplier supplier = supplierOptional.get();
            if (currentUser.getId().equals(supplier.getId())) {

                String addressName = supplier.getAddress().split(SEPARATOR)[0];
                String addressStreet = supplier.getAddress().split(SEPARATOR)[1];
                String addressCode = supplier.getAddress().split(SEPARATOR)[2];
                String addressCity = supplier.getAddress().split(SEPARATOR)[3];

                if (!addressName.equals(dto.getAddressName()) ||
                        !addressStreet.equals(dto.getAddressStreet()) ||
                        !addressCode.equals(dto.getAddressCode()) ||
                        !addressCity.equals(dto.getAddressCity())) {
                    supplier.setAddress(dto.getAddressName() + SEPARATOR + dto.getAddressStreet() + SEPARATOR + dto.getAddressCode() + SEPARATOR + dto.getAddressCity());
                }
                if (supplier.getUrl() != null) {
                    supplier.setUrl(dto.getUrl());
                }
                if (supplier.getName() != null) {
                    supplier.setName(dto.getName());
                }

                supplier = supplierRepository.save(supplier);
                return Optional.of(supplier);
            }
        }
        return Optional.empty();
    }

    public boolean approve(Long id, Employee currentUser) {
        Optional<Supplier> supplierOptional = getSupplierById(id);
        if (supplierOptional.isPresent()) {
            Supplier supplier = supplierOptional.get();
            if (loginService.isAdmin()) {
                supplier.setAwaitingApproval(false);

                supplierRepository.save(supplier);
                return true;
            }
        }

        return false;
    }
}
