package com.aps.ordering.service;

import com.aps.ordering.model.EmployeeRole;
import com.aps.ordering.model.dto.ChooseRoleDto;
import com.aps.ordering.repository.EmployeeRepository;
import com.aps.ordering.repository.EmployeeRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeRoleService {
    private final static String[] AVAILABLE_ROLES = {"ROLE_ADMIN", "ROLE_OFFICE_USER", "ROLE_PROTO_USER"};

    @Autowired
    private EmployeeRoleRepository employeeRoleRepository;

    public List<EmployeeRole> getAll() {
        return employeeRoleRepository.findAll();
    }

    public ChooseRoleDto getRolesToSelect() {
        ChooseRoleDto chooseRoleDto = new ChooseRoleDto();

        Optional<EmployeeRole> roleAdmin = employeeRoleRepository.findByName("ROLE_ADMIN");
        if (roleAdmin.isPresent()) {
            EmployeeRole role = roleAdmin.get();
            chooseRoleDto.setRole_admin("");
            chooseRoleDto.setRole_admin_id(role.getId());
            chooseRoleDto.setRole_admin_name("Admin");
        }
        Optional<EmployeeRole> roleProto = employeeRoleRepository.findByName("ROLE_PROTO_USER");
        if (roleProto.isPresent()) {
            EmployeeRole role = roleProto.get();
            chooseRoleDto.setRole_proto("");
            chooseRoleDto.setRole_proto_id(role.getId());
            chooseRoleDto.setRole_proto_name("Production");
        }
        Optional<EmployeeRole> roleOffice = employeeRoleRepository.findByName("ROLE_OFFICE_USER");
        if (roleOffice.isPresent()) {
            EmployeeRole role = roleOffice.get();
            chooseRoleDto.setRole_office("");
            chooseRoleDto.setRole_office_id(role.getId());
            chooseRoleDto.setRole_office_name("Office");
        }
        return chooseRoleDto;
    }

}
