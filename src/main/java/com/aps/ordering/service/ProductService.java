package com.aps.ordering.service;

import com.aps.ordering.repository.ProductRepository;
import com.aps.ordering.repository.SupplierRepository;
import com.aps.ordering.model.Employee;
import com.aps.ordering.model.Product;
import com.aps.ordering.model.Supplier;
import com.aps.ordering.model.dto.FormResult;
import com.aps.ordering.model.dto.ProductFormDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Value("${product.default.approved}")
    private Boolean defaultApproved;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private LoginService loginService;

    @Autowired
    private SupplierRepository supplierRepository;

    public FormResult add(ProductFormDto dto) {
        // todo : logika szukająca produktu o tej samej nazwie i weryfikująca z użytkownikiem czy wybrany produkt pasuje do podanego na liście.
        Optional<Supplier> supplier = supplierRepository.findById(dto.getSupplierId());
        List<Product> similarProducts = productRepository.findAllByNameContaining(dto.getName());

        if (!supplier.isPresent()) {
            return FormResult.failed();
        }
        Product product = new Product();
        product.setCreator(loginService.getLoggedInUser().get());
        product.setName(dto.getName());
        product.setAwaitingApproval(defaultApproved);
        product.getSuppliers().add(supplier.get());
        product.setUrl(dto.getUrl());
        product.setArchived(false);

        productRepository.save(product);

        // todo: merge products if they are similar.
        // todo: return merge result to controller

        return FormResult.created();
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public boolean approve(Long id, Employee currentUser) {
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isPresent()) {
            Product product = productOptional.get();
            product.setAwaitingApproval(false);

            productRepository.save(product);

            return true;
        }
        return false;
    }

    public Product find(Long productId) {
        return productRepository.getOne(productId);
    }
}
