package com.aps.ordering.service;

import com.aps.ordering.model.EmailModel;
import com.aps.ordering.model.EventFactory;
import com.aps.ordering.repository.ApplicationEventRepository;
import com.aps.ordering.repository.OrderListRepository;
import com.aps.ordering.model.Employee;
import com.aps.ordering.model.OrderList;
import com.aps.ordering.model.dto.OrderListFormDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class OrderListService {
    @Autowired
    private OrderListRepository orderListRepository;

    @Autowired
    private ApplicationEventRepository applicationEventRepository;

    @Autowired
    private LoginService loginService;

    @Autowired
    private MailingService mailingService;

    public List<OrderList> getAll() {
        return orderListRepository.findAll();
    }

    public void addList(OrderListFormDto dto) {
        OrderList newOrderList = new OrderList();
        newOrderList.setCreator(loginService.getLoggedInUser().get());
        newOrderList.setArchived(false);
        newOrderList.setName(dto.getOrderListName());
        newOrderList.setCreationTime(LocalDateTime.now());

        orderListRepository.save(newOrderList);
    }

    public boolean remove(Long id, Employee currentUser) {
        Optional<OrderList> orderListOptional = orderListRepository.findById(id);
        if (orderListOptional.isPresent()) {
            if (loginService.isAdmin() || orderListOptional.get().getCreator().getId() == currentUser.getId()) {
                applicationEventRepository.saveAndFlush(EventFactory.eventMessage("User removed order list:" + orderListOptional.get(), currentUser.getUsername()));
                orderListRepository.deleteById(id);
                return true;
            } else {
                // todo: log unauthorized user
            }
        } else {
            applicationEventRepository.saveAndFlush(EventFactory.eventMessage("User tried to remove non existing order list.", currentUser.getUsername()));
        }
        return false;
    }

    public Optional<OrderList> findById(Long id) {
        return orderListRepository.findById(id);
    }

    public void update(OrderList orderList) {
        orderListRepository.save(orderList);
    }

    @Transactional
    public boolean checkIfListChanged(Long id, Long entityId, Integer originalState) {
        List<EmailModel> emailModels = mailingService.findAllEntitiesById(entityId, "ORDER_LIST");

        EmailModel emailModel = emailModels.get(0);
        if (id.equals(emailModel.getEntityId())) {
            // to jest ostatni email.
            emailModels.remove(emailModel);

            mailingService.remove(emailModels);
        }

        Optional<OrderList> orderList = orderListRepository.findById(entityId);
        if (orderList.isPresent()) {
            if (orderList.get().getProductRequests().size() != originalState) {
                return false;
            } else {
                mailingService.hasBeenSent(id);
            }
        }

        return true;
    }
}
