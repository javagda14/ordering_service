package com.aps.ordering.service;

import com.aps.ordering.model.EmailModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

// ten serwis rozsyła
@Service
public class EmailQueueService {
    private final static Logger log = LoggerFactory.getLogger(EmailQueueService.class);
    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private MailingService mailingService;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private OrderListService orderListService;

    @Scheduled(fixedRate = (60000))
    public void sendAllQuickEmails() {
        List<EmailModel> emailModels = mailingService.getAllQuickEmailsToSend();
        if (emailModels.isEmpty()) {
            log.info("Nothing to send - quick queue.");
        }
        loopThroughEmails(emailModels);
    }

    @Scheduled(fixedRate = (2 * 60000))
    public void sendAllEmails() {
        List<EmailModel> emailModels = mailingService.getAllEmailsToSend();
        if (emailModels.isEmpty()) {
            log.info("Nothing to send.");
        }
        loopThroughEmails(emailModels);
    }

    private void loopThroughEmails(List<EmailModel> emailModels) {
        for (EmailModel mail : emailModels) {
            try {
                if (check(mail)) {
                    send(mail);

                    mailingService.hasBeenSent(mail.getId());
                    log.info("Email has been sent and removed from db.");
                } else {
                    log.warn("Email " + mail + " is not valid.");
                }
            } catch (Exception e) {
                log.error("Error sending email.");
            }
        }
    }

    private boolean check(EmailModel mail) {
        if (mailingService.checkIfValid(mail)) {
            if (mail.getEmailEntity() != null) {
                if (mail.getEmailEntity().equals("ORDER_LIST")) {
                    return orderListService.checkIfListChanged(mail.getId(), mail.getEntityId(), mail.getOriginalState());
                }
            }
            return true;
        }
        return false;
    }

    private void send(EmailModel emailModel) throws Exception {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setTo(emailModel.getEmailToSendTo());
        helper.setSubject(emailModel.getEmailTitle());
        helper.setText(emailModel.getContent());
        helper.setFrom(from);
        javaMailSender.send(mimeMessage);
    }
}
