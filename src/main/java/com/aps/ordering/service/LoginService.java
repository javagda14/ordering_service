package com.aps.ordering.service;

import com.aps.ordering.model.EventFactory;
import com.aps.ordering.repository.ApplicationEventRepository;
import com.aps.ordering.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class LoginService implements UserDetailsService {
    @Autowired
    private ApplicationEventRepository applicationEventRepository;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private HttpServletRequest request;
    private boolean admin;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Employee> employeeOptional = employeeService.findByUsername(username);
        if (employeeOptional.isPresent()) {
            Employee employee = employeeOptional.get();

            applicationEventRepository.save(EventFactory.loginSuccess(username, request.getRemoteAddr()));

            return User
                    .withUsername(employee.getUsername())
                    .password(employee.getPassword())
                    .roles(extractRoles(employee))
                    .build();
        }

//        applicationEventRepository.saveAndFlush(EventFactory.loginFailed(username, request.getRemoteAddr()));
        throw new UsernameNotFoundException("User not found by name: " + username);
    }

    private String[] extractRoles(Employee employee) {
        List<String> roles = employee.getRoles().stream().map(role -> role.getName().replace("ROLE_", "")).collect(Collectors.toList());
        String[] rolesArray = new String[roles.size()];
        rolesArray = roles.toArray(rolesArray);

        return rolesArray;
    }

    public Optional<Employee> getLoggedInUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            // nie jesteśmy zalogowani
            return Optional.empty();
        }

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return employeeService.findByUsername(user.getUsername());
            // jesteśmy zalogowani, zwracamy user'a
        }

        return Optional.empty();
    }

    public boolean isAdmin() {
        return getLoggedInUser().get().getRoles().stream().anyMatch(
                employeeRole -> employeeRole.getName().equals("ROLE_ADMIN"));
    }
}
