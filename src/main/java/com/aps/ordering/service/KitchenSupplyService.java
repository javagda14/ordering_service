package com.aps.ordering.service;

import com.aps.ordering.model.KitchenSupply;
import com.aps.ordering.model.KitchenSuppplyRequirement;
import com.aps.ordering.repository.KitchenSupplyRepository;
import com.aps.ordering.repository.KitchenSuppplyRequirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KitchenSupplyService {
    @Autowired
    private KitchenSuppplyRequirementRepository kitchenSuppplyRequirementRepository;

    @Autowired
    private KitchenSupplyRepository kitchenSupplyRepository;

    public List<KitchenSupply> getAllSupplies() {
        return kitchenSupplyRepository.findAll();
    }

    public List<KitchenSuppplyRequirement> getAllRequirements() {
        return kitchenSuppplyRequirementRepository.findAll();
    }
}
