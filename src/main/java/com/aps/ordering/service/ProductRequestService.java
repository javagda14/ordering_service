package com.aps.ordering.service;

import com.aps.ordering.model.OrderList;
import com.aps.ordering.model.Product;
import com.aps.ordering.model.ProductRequest;
import com.aps.ordering.model.dto.RequestFormDto;
import com.aps.ordering.model.factories.EmailFactory;
import com.aps.ordering.repository.ProductRequestRepository;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductRequestService {
    @Autowired
    private ProductRequestRepository productRequestRepository;

    @Autowired
    private OrderListService orderListService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private ProductService productService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private MailingService mailingService;

    public Optional<ProductRequest> add(RequestFormDto dto) {
        Optional<OrderList> orderListOptional = orderListService.findById(dto.getOrderId());
        if (orderListOptional.isPresent()) {
            OrderList orderList = orderListOptional.get();

            ProductRequest productRequest = new ProductRequest();
            productRequest.setDescription(dto.getDescription());
            productRequest.setAlternativeUrl(dto.getAlternativeUrl());
            productRequest.setOwner(loginService.getLoggedInUser().get());
            productRequest.setProduct(productService.find(dto.getProductId()));
            productRequest.setQuantity(dto.getQuantity());
            productRequest.setOrderList(orderList);
            productRequest.setUnit(unitService.find(dto.getUnitId()));

            productRequest = productRequestRepository.save(productRequest);

            orderList.getProductRequests().add(productRequest);
            mailingService.sendEmail(EmailFactory.orderListHasBeenModified(
                    orderList.getCreator(), orderList));

            orderListService.update(orderList);

            return Optional.of(productRequest);
        }
        return Optional.empty();
    }
}
