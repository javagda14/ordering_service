package com.aps.ordering.service;

import com.aps.ordering.model.EmployeeRole;
import com.aps.ordering.repository.EmployeeRepository;
import com.aps.ordering.repository.EmployeeRoleRepository;
import com.aps.ordering.model.Employee;
import com.aps.ordering.model.dto.EmployeeModifyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private EmployeeRoleRepository employeeRoleRepository;

    public Optional<Employee> findByUsername(String username) {
        return employeeRepository.findByUsername(username);
    }

    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Optional<Employee> register(EmployeeModifyDto dto) {
        Optional<Employee> optionalEmployee = employeeRepository.findByUsername(dto.getUsername());
        if (optionalEmployee.isPresent()) {
            return Optional.empty();
        }

        Employee newEmployee = new Employee();
        newEmployee.setName(dto.getName());
        newEmployee.setEmail(dto.getEmail());
        newEmployee.setPassword(encoder.encode(dto.getPassword()));
        newEmployee.setUsername(dto.getUsername());
        newEmployee.setSurname(dto.getSurname());
        newEmployee.setAccountId(UUID.randomUUID().toString());
        newEmployee.setRoles(new HashSet<>(Arrays.asList(employeeRoleRepository.findByName("ROLE_USER").get())));

        if (dto.getRoles().getRole_admin() != null) {
            Optional<EmployeeRole> employeeRole = employeeRoleRepository.findByName("ROLE_ADMIN");
            if (employeeRole.isPresent()) {
                newEmployee.getRoles().add(employeeRole.get());
            }
        }
        if (dto.getRoles().getRole_office() != null) {
            Optional<EmployeeRole> employeeRole = employeeRoleRepository.findByName("ROLE_OFFICE_USER");
            if (employeeRole.isPresent()) {
                newEmployee.getRoles().add(employeeRole.get());
            }
        }
        if (dto.getRoles().getRole_proto() != null) {
            Optional<EmployeeRole> employeeRole = employeeRoleRepository.findByName("ROLE_PROTO_USER");
            if (employeeRole.isPresent()) {
                newEmployee.getRoles().add(employeeRole.get());
            }
        }

        newEmployee = employeeRepository.save(newEmployee);

        return Optional.of(newEmployee);
    }

    public boolean activate(String accountId) {
        Optional<Employee> employee = employeeRepository.findByAccountId(accountId);
        if (employee.isPresent()) {
            Employee em = employee.get();
            em.setAccountId("");

            employeeRepository.save(em);
            return true;
        }
        return false;
    }
}
