package com.aps.ordering.controller;

import com.aps.ordering.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/activate/")
public class AccountActivationController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/account/{account_id}")
    public String activate(@PathVariable(name = "account_id") String accountId, Model model) {
        model.addAttribute("success", employeeService.activate(accountId));
        return "activation";
    }
}
