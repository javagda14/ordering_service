package com.aps.ordering.controller.admin;

import com.aps.ordering.model.Employee;
import com.aps.ordering.model.dto.EmployeeModifyDto;
import com.aps.ordering.model.factories.ActivationLinkGenerator;
import com.aps.ordering.model.factories.EmailFactory;
import com.aps.ordering.service.EmployeeRoleService;
import com.aps.ordering.service.EmployeeService;
import com.aps.ordering.service.MailingService;
import com.aps.ordering.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping("/admin/employee/")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminEmployeeController {

    @Autowired
    private ActivationLinkGenerator activationLinkGenerator;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private MailingService mailingService;

    @Autowired
    private SmsService smsService;

    @Autowired
    private EmployeeRoleService employeeRoleService;

    @GetMapping("/list")
    public String employeeList(Model model) {
        model.addAttribute("employeeList", employeeService.getAll());
        return "admin/employee/list";
    }

    @GetMapping("/add")
    public String getEmployeeForm(Model model) {
        EmployeeModifyDto dto = new EmployeeModifyDto();
        dto.setRoles(employeeRoleService.getRolesToSelect());
        model.addAttribute("employee", dto);

        return "admin/employee/employeeForm";
    }

    @PostMapping("/add")
    public String sendEmployeeForm(EmployeeModifyDto dto, Model model) {
        Optional<Employee> employeeOptional = employeeService.register(dto);
        if (employeeOptional.isPresent()) {
            Employee employee = employeeOptional.get();
            mailingService.sendQuickEmail(
                    EmailFactory.createRegistrationConfirmationEmail(employee, activationLinkGenerator.hostAddress()));

            return "redirect:/admin/employee/list";
        }

        return "redirect:/admin/employee/list";
    }

    @GetMapping("/user/{id}")
    public String getEmployeeProfile(Model model) {

        return "admin/employee/profile";
    }

}
