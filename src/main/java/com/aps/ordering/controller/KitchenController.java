package com.aps.ordering.controller;


import com.aps.ordering.model.dto.AddKitchenSupplyDto;
import com.aps.ordering.service.KitchenSupplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/kitchen/")
@PreAuthorize("hasRole('ROLE_USER')")
public class KitchenController {

    @Autowired
    private KitchenSupplyService kitchenSupplyService;

    @GetMapping(path = "/list")
    public String list(Model model) {
        model.addAttribute("requirements", kitchenSupplyService.getAllRequirements());

        return "kitchen/list";
    }

    @GetMapping(path = "/add")
    public String add(Model model) {
        model.addAttribute("supplies", kitchenSupplyService.getAllSupplies());

        return "kitchen/add";
    }

    @PostMapping(path = "/add")
    public String add(Model model, AddKitchenSupplyDto dto) {

        return "redirect:/kitchen/list";
    }


}
