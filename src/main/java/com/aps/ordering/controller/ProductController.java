package com.aps.ordering.controller;

import com.aps.ordering.service.LoginService;
import com.aps.ordering.service.ProductService;
import com.aps.ordering.service.SupplierService;
import com.aps.ordering.service.UnitService;
import com.aps.ordering.util.MessageUtil;
import com.aps.ordering.model.Employee;
import com.aps.ordering.model.dto.FormResult;
import com.aps.ordering.model.dto.ProductFormDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/product/")
public class ProductController {

    @Value("${product.default.approved}")
    private Boolean defaultApproved;

    @Autowired
    private ProductService productService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private SupplierService supplierService;

    @ModelAttribute("currentUser")
    public Employee getCurrentUser() {
        return loginService.getLoggedInUser().get();
    }

    @ModelAttribute("admin")
    public boolean getIsAdmin() {
        return loginService.isAdmin();
    }

    @GetMapping("/list/")
    public String getList(Model model, RedirectAttributes redirectAttributes) {
        model.addAttribute("productList", productService.findAll());

        MessageUtil.addMessageToModel(model, redirectAttributes);

        return "product/list";
    }

    @GetMapping("/list")
    public String getList(Model model, RedirectAttributes redirectAttributes,
                          @RequestParam(name = "message", required = false) String message,
                          @RequestParam(name = "error_message", required = false) String error_message) {
        MessageUtil.addMessageToRedirectAttributes(redirectAttributes, message, error_message);

        return "redirect:/product/list/";
    }

    @GetMapping("/add")
    public String getProductForm(Model model, @RequestParam(name = "error", required = false) String error) {
        model.addAttribute("productForm", new ProductFormDto());
        model.addAttribute("suppliers", supplierService.findAll());
        model.addAttribute("units", unitService.listAll());
        if (error != null) {
            model.addAttribute("error_message", "Unable to add this product.");
        }

        return "product/productForm";
    }

    @GetMapping("/approve/{id}")
    public String approve(@PathVariable(name = "id") Long id) {
        if (productService.approve(id, getCurrentUser())) {
            return "redirect:/product/list?message=Product has been approved!";
        }
        return "redirect:/product/list?error_message=Product coudn't have been approved";
    }

    @PostMapping("/add")
    public String add(Model model, ProductFormDto dto) {
        FormResult formResult = productService.add(dto);
        if (formResult.getStatus() == FormResult.FORM_RESULT_STATUS.CREATED) {
            if (!defaultApproved) {
                return "redirect:/product/list?message=" + "Newly added product has to be approved by admin.";
            } else {
                return "redirect:/product/list?message=" + "Product has been added.";
            }
        } else if (formResult.getStatus() == FormResult.FORM_RESULT_STATUS.MERGE) {
            // todo: merge page
        }

        return "redirect:/product/add?error";
    }
}