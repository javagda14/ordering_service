package com.aps.ordering.controller;

import com.aps.ordering.model.ProductRequest;
import com.aps.ordering.model.Supplier;
import com.aps.ordering.model.dto.RequestFormDto;
import com.aps.ordering.service.ProductRequestService;
import com.aps.ordering.service.ProductService;
import com.aps.ordering.service.SupplierService;
import com.aps.ordering.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping("/request/")
public class RequestController {

    @Autowired
    private ProductRequestService productRequestService;

    @Autowired
    private ProductService productService;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private UnitService unitService;

    @GetMapping("/add/{order_list}")
    public String getAddForm(Model model, @PathVariable(name = "order_list") Long orderListId) {
        model.addAttribute("products", productService.findAll());
        model.addAttribute("suppliers", supplierService.findAll());
        model.addAttribute("units", unitService.listAll());

        RequestFormDto dto = new RequestFormDto();
        dto.setOrderId(orderListId);
        model.addAttribute("requestForm", dto);

        return "request/requestForm";
    }

    @PostMapping("/add")
    public String submitRequest(Model model, RequestFormDto dto) {
        Optional<ProductRequest> productRequestOptional = productRequestService.add(dto);


        return "redirect:/order/details/" + dto.getOrderId();
    }
}
