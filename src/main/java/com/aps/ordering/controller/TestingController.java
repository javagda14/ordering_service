package com.aps.ordering.controller;

import com.aps.ordering.model.dto.TestDto;
import com.aps.ordering.model.dto.TestElementDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/test/")
public class TestingController {

    @GetMapping("/form")
    public String form(Model model) {
        List<TestElementDto> list = new ArrayList<>(Arrays.asList(
                new TestElementDto(1L, "aaa", false),
                new TestElementDto(2L, "bbb", true),
                new TestElementDto(3L, "ccc", false)));
        model.addAttribute("task",
                new TestDto("Some tam jakis tekst do wyswietlenia", list));

        model.addAttribute("elementDtoList", list);
        return "test";
    }

    @PostMapping("/form")
    public String formPost(Model model, TestDto testDto) {
        System.out.println(testDto);

        return "redirect:/test/form";
    }
}
