package com.aps.ordering.controller;

import com.aps.ordering.model.dto.OrderListFormDto;
import com.aps.ordering.service.LoginService;
import com.aps.ordering.service.OrderListService;
import com.aps.ordering.util.MessageUtil;
import com.aps.ordering.model.Employee;
import com.aps.ordering.model.OrderList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/order/")
@PreAuthorize(value = "hasRole('ROLE_USER')")
public class OrderListController {

    @Value("${order.empty.lists.count}")
    private int MAX_EMPTY_LISTS;

    @Autowired
    private OrderListService orderListService;

    @Autowired
    private LoginService loginService;

    @ModelAttribute("currentUser")
    public Employee getCurrentUser() {
        return loginService.getLoggedInUser().get();
    }

    @ModelAttribute("admin")
    public boolean getIsAdmin() {
        return loginService.isAdmin();
    }

    @GetMapping("/list/")
    public String getList(Model model, RedirectAttributes redirectAttributes) {
        List<OrderList> orderListList = orderListService.getAll();
        model.addAttribute("orderLists", orderListList);
        model.addAttribute("orderForm", new OrderListFormDto());
        model.addAttribute("allow_new_lists", orderListList.stream().filter(list -> list.getProductRequests().isEmpty()).count() < MAX_EMPTY_LISTS);

        MessageUtil.addMessageToModel(model, redirectAttributes);

        return "order/list";
    }

    @GetMapping("/list")
    public String getList(Model model, RedirectAttributes redirectAttributes,
                          @RequestParam(name = "message", required = false) String message) {
        MessageUtil.addMessageToRedirectAttributes(redirectAttributes, message);

        return "redirect:/order/list/";
    }

    @PostMapping("/add")
    public String sendNewListForm(Model model, OrderListFormDto dto) {
        orderListService.addList(dto);

        return "redirect:/order/list?message=Order list has been added";
    }

    @GetMapping("/remove/{id}")
    public String removeOrderList(@PathVariable(name = "id") Long id) {
        boolean result = orderListService.remove(id, getCurrentUser());
        if (result) {
            return "redirect:/order/list?message=Order list has been removed.";
        }
        return "redirect:/order/list?error_message=Unable to remove this order list!";
    }

    @GetMapping("/details/{id}")
    public String getDetailsPage(@PathVariable(name = "id") Long id, Model model) {
        Optional<OrderList> orderListOptional = orderListService.findById(id);
        if (orderListOptional.isPresent()) {
            OrderList orderList = orderListOptional.get();
            model.addAttribute("detailed_order_list", orderList);
            model.addAttribute("orders", orderList.getProductRequests());

            return "order/orderDetails";
        }
        return "rediredt:/order/list?error_message=Unable to view this order list!";
    }
}
