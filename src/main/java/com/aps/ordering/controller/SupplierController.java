package com.aps.ordering.controller;

import com.aps.ordering.service.LoginService;
import com.aps.ordering.service.SupplierService;
import com.aps.ordering.model.Employee;
import com.aps.ordering.model.Supplier;
import com.aps.ordering.model.dto.SupplierFormDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping("/supplier/")
@PreAuthorize("hasRole('ROLE_USER')")
public class SupplierController {

    @Value("${supplier.default.approved}")
    private Boolean defaultApproved;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private LoginService loginService;

    @ModelAttribute("currentUser")
    public Employee getCurrentUser() {
        return loginService.getLoggedInUser().get();
    }

    @ModelAttribute("admin")
    public boolean getIsAdmin() {
        return loginService.isAdmin();
    }

    @GetMapping("/list")
    public String getList(Model model, @RequestParam(name = "message", required = false) String message,
                          @RequestParam(name = "error_message", required = false) String error_message) {
        model.addAttribute("supplierList", supplierService.findAll());

        if (message != null) {
            model.addAttribute("message", message);
        }
        if (error_message != null) {
            model.addAttribute("error_message", error_message);
        }
        return "supplier/list";
    }

    @GetMapping("/add")
    public String getCreationForm(Model model) {
        model.addAttribute("supplierForm", new SupplierFormDto());
        return "supplier/supplierForm";
    }


    @GetMapping("/edit/{id}")
    public String getList(Model model, @PathVariable(name = "id", required = true) Long id) {
        Optional<Supplier> supplierOptional = supplierService.getSupplierById(id);
        if (supplierOptional.isPresent()) {
            Supplier supplier = supplierOptional.get();

            Employee currentUser = getCurrentUser();

            if (loginService.isAdmin() || supplier.getCreator().getId().equals(currentUser.getId())) {
                model.addAttribute("supplierForm", SupplierFormDto.createFrom(supplierOptional.get()));
            } else {
                return "redirect:/supplier/list?error_message=You cannot edit given supplier.";
            }
        } else {
            return "redirect:/supplier/list?error_message=Supplier does not exist.";
        }

        return "supplier/editForm";
    }

    @PostMapping("/edit/{id}")
    public String modify(SupplierFormDto dto, @PathVariable(name = "id", required = true) Long id) {
        Optional<Supplier> supplierOptional = supplierService.modify(id, dto, getCurrentUser());
        if (supplierOptional.isPresent()) {

        }
        return "redirect:/supplier/details/" + id;
    }

    @PostMapping("/add")
    public String add(Model model, SupplierFormDto dto) {
        if (supplierService.add(dto)) {

            if (!defaultApproved) {
                return "redirect:/supplier/list?message=" + "Newly added supplier has to be approved by admin.";
            } else {
                return "redirect:/supplier/list?message=" + "Supplier has been added.";
            }
        }

        model.addAttribute("error_message", "Unable to add supplier");
        model.addAttribute("supplierForm", new SupplierFormDto());
        model.addAttribute("supplierList", supplierService.findAll());

        return "supplier/list";
    }

    @GetMapping("/remove/{id}")
    public String remove(@PathVariable(name = "id", required = true) Long id) {
        if (supplierService.remove(id, getCurrentUser())) {

        } else {

        }
        return "redirect:/supplier/list";
    }

    @GetMapping("/approve/{id}")
    public String approve(@PathVariable(name = "id") Long id) {
        if (supplierService.approve(id, getCurrentUser())) {
            return "redirect:/supplier/list?message=Supplier has been approved!";
        }
        return "redirect:/supplier/list?error_message=Supplier coudn't have been approved";
    }

    @GetMapping("/details/{id}")
    public String getDetails(Model model, @PathVariable(name = "id") Long id) {
        Optional<Supplier> optionalSupplier = supplierService.getSupplierById(id);
        if (optionalSupplier.isPresent()) {
            Supplier supplier = optionalSupplier.get();

            model.addAttribute("supplier", supplier);

            return "supplier/details";
        }
        return "redirect:/supplier/list?error_message=Given supplier does not exist.";
    }
}
