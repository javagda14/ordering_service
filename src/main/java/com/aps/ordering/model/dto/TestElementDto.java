package com.aps.ordering.model.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TestElementDto {
    private Long id;
    private String text;
    private boolean checked;
}
