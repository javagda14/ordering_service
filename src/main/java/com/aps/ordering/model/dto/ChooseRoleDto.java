package com.aps.ordering.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChooseRoleDto {
    private Long role_office_id;
    private Long role_proto_id;
    private Long role_admin_id;

    private String role_office;
    private String role_proto;
    private String role_admin;

    private String role_office_name;
    private String role_proto_name;
    private String role_admin_name;
}
