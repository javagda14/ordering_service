package com.aps.ordering.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderListFormDto {
    @NotNull
    @Size(min = 4, max = 250)
    private String orderListName;
}
