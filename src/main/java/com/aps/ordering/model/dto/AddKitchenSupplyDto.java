package com.aps.ordering.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddKitchenSupplyDto {
    private Long kitchenSupplyId;
    private Double quantity;
}
