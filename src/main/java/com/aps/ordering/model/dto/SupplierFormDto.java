package com.aps.ordering.model.dto;

import com.aps.ordering.model.Supplier;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SupplierFormDto {

    private Long modifiedId;

    @NotNull
    @NotEmpty
    private String name;

    private String addressName;
    private String addressStreet;
    private String addressCode;
    private String addressCity;

    @NotEmpty
    @URL
    private String url;

    public static SupplierFormDto createFrom(Supplier supplier) {
        return SupplierFormDto
                .builder()
                .modifiedId(supplier.getId())
                .addressName(supplier.getAddress().split(";")[0])
                .addressStreet(supplier.getAddress().split(";")[1])
                .addressCode(supplier.getAddress().split(";")[2])
                .addressCity(supplier.getAddress().split(";")[3])
                .name(supplier.getName())
                .url(supplier.getUrl())
                .build();
    }
}
