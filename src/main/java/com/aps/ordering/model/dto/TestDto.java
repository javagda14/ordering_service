package com.aps.ordering.model.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TestDto {
    private String jakisTekst;
    private List<TestElementDto> elementDtoList;
}
