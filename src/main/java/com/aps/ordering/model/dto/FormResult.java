package com.aps.ordering.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Builder
@Data
@AllArgsConstructor
public class FormResult {
    private FORM_RESULT_STATUS status;
    private Long database_instance;
    private Long created_instance;

    public static enum FORM_RESULT_STATUS {
        CREATED, FAILED, MERGE;
    }

    public static FormResult failed() {
        return FormResult.builder().status(FORM_RESULT_STATUS.FAILED).build();
    }

    public static FormResult created() {
        return FormResult.builder().status(FORM_RESULT_STATUS.CREATED).build();
    }

    public static FormResult merge(Long dbI, Long createdI) {
        return FormResult.builder().status(FORM_RESULT_STATUS.MERGE).created_instance(createdI).database_instance(dbI).build();
    }
}
