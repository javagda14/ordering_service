package com.aps.ordering.model.dto;

import com.aps.ordering.model.Unit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductFormDto {
    @NotEmpty
    private String name;

    @NotEmpty
    private String url;

    private Long supplierId;

//    private Unit unitId;
}
