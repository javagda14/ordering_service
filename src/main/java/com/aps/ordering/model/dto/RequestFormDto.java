package com.aps.ordering.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequestFormDto {
    private double quantity;

    private String alternativeUrl;
    private String description;
    private Long orderId;
    private Long productId;
    private Long unitId;

}
