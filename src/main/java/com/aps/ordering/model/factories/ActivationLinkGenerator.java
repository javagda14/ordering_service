package com.aps.ordering.model.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

@Component
public class ActivationLinkGenerator {

    @Autowired
    private Environment environment;

    public String address() {
        try {
            Enumeration<NetworkInterface> enumeration = NetworkInterface.getNetworkInterfaces();
            while (enumeration.hasMoreElements()) {
                NetworkInterface networkInterface = enumeration.nextElement();
                if (networkInterface.isLoopback()) {
                    continue;
                }
                Enumeration<InetAddress> addressEnumeration = networkInterface.getInetAddresses();
                while (addressEnumeration.hasMoreElements()) {
                    InetAddress inetAddress = addressEnumeration.nextElement();
                    if (inetAddress.isLoopbackAddress()) {
                        continue;
                    }
                    if (inetAddress.getHostAddress().contains("127.0.0.1") ||
                            inetAddress.getHostAddress().contains("localhost") ||
                            inetAddress.getHostAddress().contains("::1") ||
                            inetAddress.getHostAddress().contains("172.17.0.1") ||
                            inetAddress.getHostAddress().contains(":")) {
                        continue;
                    }
                    if (inetAddress.getHostAddress().contains("eth1")) {
                        return inetAddress.getHostAddress().split("%")[0];
                    }
                    if (inetAddress.getHostAddress().contains("%")) {
                        return inetAddress.getHostAddress().split("%")[0];
                    }
                    return inetAddress.getHostAddress();
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return "10.2.0.67";
    }

    public String hostAddress() {
        String host = address();
        Integer port = environment.getProperty("server.port", Integer.class, 8080).intValue();
        return "http://" + host + ":" + port + "/activate/account/";
    }

}
