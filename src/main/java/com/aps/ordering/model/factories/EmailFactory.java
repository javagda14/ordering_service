package com.aps.ordering.model.factories;

import com.aps.ordering.model.EmailModel;
import com.aps.ordering.model.Employee;
import com.aps.ordering.model.OrderList;

public abstract class EmailFactory {
    public static EmailModel createRegistrationConfirmationEmail(Employee employee, String host) {
        return new EmailModel(null, employee.getEmail(),
                "Account has been created for You. " +
                        "Your login is: " + employee.getUsername() + "\n" +
                        "To Activate: " + host + employee.getAccountId() + " <---- go! ",
                "Account registered!", true, null, null, null);
    }

    public static EmailModel orderListHasBeenModified(Employee owner, OrderList orderList) {
        return new EmailModel(null,
                owner.getEmail(),
                "Order list has been changed!",
                "Order changed",
                false,
                "ORDER_LIST",
                orderList.getId(),
                orderList.getProductRequests().size());
    }
}
