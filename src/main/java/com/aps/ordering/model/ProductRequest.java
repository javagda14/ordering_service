package com.aps.ordering.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.*;

/**
 * Pozycja na liście (z quantity)
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private OrderList orderList;

    @ManyToOne
    private Product product;

    private double quantity;

    private String alternativeUrl;
    private String description;

    @ManyToOne
    @JsonIgnore
    private Unit unit;

    @ManyToOne
    private Employee owner;

    @Override
    public String toString() {
        return "ProductRequest{" +
                "id=" + id +
                ", orderList=" + orderList +
                ", product=" + product +
                ", quantity=" + quantity +
                ", alternativeUrl='" + alternativeUrl + '\'' +
                ", description='" + description + '\'' +
                ", owner=" + owner +
                '}';
    }
}
