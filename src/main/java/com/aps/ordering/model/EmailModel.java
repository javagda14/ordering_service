package com.aps.ordering.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmailModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String emailToSendTo;
    private String content;
    private String emailTitle;
    private boolean shouldBeSentQuickly;

    private String emailEntity; // order list
    private Long entityId; // number of elements - i.e. 3
    private Integer originalState; // number of elements - i.e. 3

}
