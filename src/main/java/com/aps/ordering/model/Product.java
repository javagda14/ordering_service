package com.aps.ordering.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Produkt do zamówienia
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private boolean archived;

    private String url;

    private boolean awaitingApproval;

    @Cascade(value = {org.hibernate.annotations.CascadeType.SAVE_UPDATE,
            org.hibernate.annotations.CascadeType.LOCK})
    @OneToMany
    private List<ProductRequest> productRequests;

    @ManyToMany
    private List<Supplier> suppliers = new ArrayList<>();

    @ManyToOne
    private Employee creator;

    @CreationTimestamp
    private LocalDateTime creationTime;

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", archived=" + archived +
                ", url='" + url + '\'' +
                ", awaitingApproval=" + awaitingApproval +
                ", creator=" + creator +
                ", creationTime=" + creationTime +
                '}';
    }
}
