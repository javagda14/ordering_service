package com.aps.ordering.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @URL
    private String url;

    private String address;

    private boolean awaitingApproval;

    @ManyToMany(mappedBy = "suppliers")
    private List<Product> productList;

    @ManyToOne
    private Employee creator;

    @Override
    public String toString() {
        return "Supplier{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", address='" + address + '\'' +
                ", awaitingApproval=" + awaitingApproval +
                ", creator=" + creator +
                '}';
    }
}
