package com.aps.ordering.model;

public final class EventFactory {
    private EventFactory() {
    }

    public static ApplicationEvent loginSuccess(String username, String address) {
        return ApplicationEvent.builder().event("Login success. IP: " + address).msg("User: " + username).build();
    }

    public static ApplicationEvent loginFailed(String username, String password, String address) {
        return ApplicationEvent.builder().event("Login failed. IP: " + address).msg("User: " + username + " used password: " + password).build();
    }

    public static ApplicationEvent eventMessage(String message, String user) {
        return ApplicationEvent.builder().event(message).msg("User: " + user).build();
    }
}
