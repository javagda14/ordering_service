package com.aps.ordering.model;


import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name, surname;

    private String username;
    private String password;

    private String email;

    private String accountId;

    @ManyToMany
    private Set<EmployeeRole> roles;

    @OneToMany(mappedBy = "creator")
    private List<OrderList> orderLists;

    @OneToMany(mappedBy = "owner")
    private List<ProductRequest> productRequests;

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
