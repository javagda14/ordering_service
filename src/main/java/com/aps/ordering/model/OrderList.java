package com.aps.ordering.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Kompletna lista życzeń
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderList {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private boolean archived;

    @OneToMany(mappedBy = "orderList")
    private List<ProductRequest> productRequests;

    @ManyToOne
    private Employee creator;

    @CreationTimestamp
    private LocalDateTime creationTime;

    @Override
    public String toString() {
        return "OrderList{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", archived=" + archived +
                ", creator=" + creator +
                ", creationTime=" + creationTime +
                '}';
    }
}
