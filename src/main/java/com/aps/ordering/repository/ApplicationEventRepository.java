package com.aps.ordering.repository;

import com.aps.ordering.model.ApplicationEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationEventRepository extends JpaRepository<ApplicationEvent, Long> {
}
