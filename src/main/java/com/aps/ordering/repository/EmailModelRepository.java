package com.aps.ordering.repository;

import com.aps.ordering.model.EmailModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.Email;
import java.util.List;

@Repository
public interface EmailModelRepository extends JpaRepository<EmailModel, Long> {

    List<EmailModel> findAllByShouldBeSentQuickly(boolean should);

    List<EmailModel> findAllByEmailEntityAndEntityId(String entity, Long entityId);
}
