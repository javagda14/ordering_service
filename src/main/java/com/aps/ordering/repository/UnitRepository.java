package com.aps.ordering.repository;

import com.aps.ordering.model.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UnitRepository extends JpaRepository<Unit, Long>{

    Optional<Unit> findByName(String unitName);
}
