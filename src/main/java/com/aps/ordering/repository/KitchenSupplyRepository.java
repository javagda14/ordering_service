package com.aps.ordering.repository;

import com.aps.ordering.model.KitchenSupply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KitchenSupplyRepository extends JpaRepository<KitchenSupply, Long> {
}
