package com.aps.ordering.repository;

import com.aps.ordering.model.KitchenSuppplyRequirement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KitchenSuppplyRequirementRepository extends JpaRepository<KitchenSuppplyRequirement, Long> {
}
