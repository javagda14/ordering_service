package com.aps.ordering.util;

import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class MessageUtil {

    public static void addMessageToModel(Model model, RedirectAttributes redirectAttributes) {
        Object messageObject = redirectAttributes.getFlashAttributes().get("message");
        if (messageObject != null) {
            String message = String.valueOf(messageObject);
            model.addAttribute("message", message);
        }
        Object error_messageObject = redirectAttributes.getFlashAttributes().get("error_message");
        if (error_messageObject != null) {
            String error_message = String.valueOf(error_messageObject);
            model.addAttribute("error_message", error_message);
        }
    }

    public static void addMessageToRedirectAttributes(RedirectAttributes redirectAttributes, String message) {
        addMessageToRedirectAttributes(redirectAttributes, message, null);
    }

    public static void addMessageToRedirectAttributes(RedirectAttributes redirectAttributes, String message, String error_message) {
        if (message != null) {
            redirectAttributes.addFlashAttribute("message", message);
        }
        if (error_message != null) {
            redirectAttributes.addFlashAttribute("error_message", error_message);
        }
    }
}
