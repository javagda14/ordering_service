package com.aps.ordering.configuration;

import org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageAutoConfiguration extends MessageSourceAutoConfiguration{
}
