package com.aps.ordering.component;

import com.aps.ordering.model.Unit;
import com.aps.ordering.repository.EmployeeRepository;
import com.aps.ordering.repository.EmployeeRoleRepository;
import com.aps.ordering.model.Employee;
import com.aps.ordering.model.EmployeeRole;
import com.aps.ordering.repository.UnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.plaf.OptionPaneUI;
import java.util.*;

@Component
public class InitialDataInitializer implements
        ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private EmployeeRoleRepository employeeRoleRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private UnitRepository unitRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        addRole("ROLE_ADMIN");
        addRole("ROLE_OFFICE_USER");
        addRole("ROLE_PROTO_USER");
        addRole("ROLE_USER");

        addUnit("szt");
        addUnit("kg");
        addUnit("mm");
        addUnit("mb.");
        addUnit("g");

        addEmployee("admin", "admin", "ROLE_ADMIN", "ROLE_USER");
        addEmployee("user", "user", "ROLE_USER");
    }

    @Transactional
    public void addUnit(String unitName) {
        Optional<Unit> unitOptional = unitRepository.findByName(unitName);
        if (!unitOptional.isPresent()) {
            unitRepository.save(new Unit(null, unitName));
        }
    }

    @Transactional
    public void addRole(String name) {
        Optional<EmployeeRole> employeeRoleOptional = employeeRoleRepository.findByName(name);
        if (!employeeRoleOptional.isPresent()) {
            employeeRoleRepository.save(new EmployeeRole(null, name));
        }
    }

    @Transactional
    public void addEmployee(String username, String password, String... roles) {
        Optional<Employee> employeeOptional = employeeRepository.findByUsername(username);
        if (!employeeOptional.isPresent()) {
            List<EmployeeRole> rolesList = new ArrayList<>();
            for (String role : roles) {
                Optional<EmployeeRole> employeeRoleOptional = employeeRoleRepository.findByName(role);
                employeeRoleOptional.ifPresent(rolesList::add);
            }

            employeeRepository.save(new Employee(
                    null, // id
                    null, // name
                    null, // surname
                    username, // username
                    passwordEncoder.encode(password), // password
                    null, // email
                    null,
                    new HashSet<EmployeeRole>(rolesList),
                    new ArrayList<>(),
                    new ArrayList<>())); // roles
        }
    }
}
